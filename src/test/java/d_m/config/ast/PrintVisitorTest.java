package d_m.config.ast;

import d_m.config.ast.expr.*;
import d_m.config.ast.stmt.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class PrintVisitorTest {
    private static ByteArrayOutputStream out = new ByteArrayOutputStream();

    @org.junit.Before
    public void setUp() throws Exception {
        System.setOut(new PrintStream(out));
    }

    @org.junit.After
    public void tearDown() throws Exception {
        out.reset();
        System.setOut(null);
    }

    @org.junit.Test
    public void testExpr() throws Exception {
        Expr varExpr = new VarExpr("var");
        Expr selectorExpr = new SelectorExpr("#hello");

        PrintVisitor visitor = new PrintVisitor(2);
        varExpr.accept(visitor);
        selectorExpr.accept(visitor);

        assertEquals(out.toString(), "var#hello");
    }

    @org.junit.Test
    public void testGoto() throws Exception {
        GotoStmt gotoStmt = new GotoStmt("www.google.com");

        PrintVisitor visitor = new PrintVisitor(2);
        gotoStmt.accept(visitor);

        assertEquals(out.toString(), "goto www.google.com\n");
    }

    @org.junit.Test
    public void testPrintf() throws Exception {
        List<Expr> expressions = new ArrayList<>();
        expressions.add(new IntExpr(1));
        expressions.add(new IntExpr(1));
        expressions.add(new IntExpr(2));
        PrintfStmt printfStmt = new PrintfStmt("{} + {} = {}", expressions);

        PrintVisitor visitor = new PrintVisitor(2);
        printfStmt.accept(visitor);

        assertEquals(out.toString(), "printf \"{} + {} = {}\", 1, 1, 2");
    }

    @org.junit.Test
    public void testBinding() throws Exception {
        Map<String, Expr> bindings = new HashMap<>();
        bindings.put("id", new IntExpr(2));
        bindings.put("age", new IntExpr(20));
        BindingStmt bindStmt = new BindingStmt(bindings);

        PrintVisitor visitor = new PrintVisitor(2);
        bindStmt.accept(visitor);

        assertEquals(out.toString(), "id = 2, age = 20\n");
    }

    @org.junit.Test
    public void testFor() throws Exception {
        Map<String, Expr> bindings1 = new HashMap<>();
        Map<String, Expr> bindings2 = new HashMap<>();
        bindings1.put("id", new IntExpr(2));
        bindings2.put("id", new IntExpr(3));

        BindingStmt bindStmt1 = new BindingStmt(bindings1);
        BindingStmt bindStmt2 = new BindingStmt(bindings2);
        SeqStmt body = new SeqStmt(bindStmt1, bindStmt2);

        Expr names = new VarExpr("names");
        ForExpr forExpr = new ForExpr(names, "name", body);

        PrintVisitor visitor = new PrintVisitor(2);
        forExpr.accept(visitor);

        assertEquals(out.toString(), "for name in names {\n  id = 2\n  id = 3\n}");
    }

    @org.junit.Test
    public void testNestedFor() throws Exception {
        Map<String, Expr> bindings1 = new HashMap<>();
        Map<String, Expr> bindings2 = new HashMap<>();
        bindings1.put("id", new IntExpr(2));
        bindings2.put("id", new IntExpr(3));

        BindingStmt bindStmt1 = new BindingStmt(bindings1);
        BindingStmt bindStmt2 = new BindingStmt(bindings2);
        SeqStmt body = new SeqStmt(bindStmt1, bindStmt2);

        ForExpr forExpr = new ForExpr(new VarExpr("names"), "name",
            new BindingStmt("res", new ForExpr(new VarExpr("values"), "value", body)));
        BindingStmt stmt = new BindingStmt("res", forExpr);

        PrintVisitor visitor = new PrintVisitor(2);
        stmt.accept(visitor);

        assertEquals(out.toString(), "res = for name in names {\n  res = for value in values {\n" +
            "    id = 2\n    id = 3\n  }\n}\n");
    }
}