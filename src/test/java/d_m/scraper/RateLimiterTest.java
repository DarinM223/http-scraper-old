package d_m.scraper;

import static org.junit.Assert.*;

public class RateLimiterTest {
    @org.junit.Test
    public void testNormal() throws Exception {
        RateLimiter rateLimiter = new RateLimiter();
        rateLimiter.addDomain("www.google.com", 3, RateLimiter.ONE_SECOND);

        assertEquals(rateLimiter.canCall("www.google.com"), true);
        assertEquals(rateLimiter.canCall("www.google.com"), true);
        assertEquals(rateLimiter.canCall("www.google.com"), true);
        assertEquals(rateLimiter.canCall("www.google.com"), false);
    }

    @org.junit.Test
    public void testSucceedAfterMinute() throws Exception {
        RateLimiter rateLimiter = new RateLimiter();
        rateLimiter.addDomain("www.google.com", 3, RateLimiter.ONE_SECOND);

        assertEquals(rateLimiter.canCall("www.google.com"), true);
        assertEquals(rateLimiter.canCall("www.google.com"), true);
        assertEquals(rateLimiter.canCall("www.google.com"), true);

        // Wait for a second
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        // Should be able to call after waiting
        assertEquals(rateLimiter.canCall("www.google.com"), true);
    }
}