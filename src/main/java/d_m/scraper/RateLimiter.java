package d_m.scraper;

import java.util.Deque;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * A thread safe rate limiter
 */
public class RateLimiter {
    public static final long ONE_MINUTE = 60000000;
    public static final long ONE_SECOND = 1000000;

    private final Map<String, Deque<Long>> domainQueues;
    private final Map<String, Integer> maxTimes;
    private final Map<String, Long> elapsedTimes;

    /**
     * Returns the difference between two time numbers in microseconds
     */
    public static long timeDifference(long currTime, long lastTime) {
        return Math.abs(currTime - lastTime) / 1000;
    }

    public RateLimiter() {
        this.domainQueues = new HashMap<>();
        this.maxTimes = new HashMap<>();
        this.elapsedTimes = new HashMap<>();
    }

    public void addDomain(String domain, int maxTimes, long elapsedTime) {
        this.domainQueues.put(domain, new ConcurrentLinkedDeque<>());
        this.maxTimes.put(domain, maxTimes);
        this.elapsedTimes.put(domain, elapsedTime);
    }

    public boolean canCall(String domain) {
        long currTime = System.nanoTime();
        Deque<Long> deque = this.domainQueues.get(domain);

        if (deque.size() > 0) {
            // Forget about last times that are after the specified period
            long lastTime = deque.peekLast();
            long elapsed = RateLimiter.timeDifference(currTime, lastTime);
            while (elapsed > this.elapsedTimes.get(domain)) {
                deque.removeLast();

                if (deque.isEmpty()) {
                    break;
                }
                lastTime = deque.peekLast();
                elapsed = RateLimiter.timeDifference(currTime, lastTime);
            }
        }

        if (deque.size() < this.maxTimes.get(domain)) {
            deque.add(currTime);
            return true;
        }
        return false;
    }
}
