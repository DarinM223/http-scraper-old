package d_m.scraper;

import d_m.scraper.message.Message;

import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static java.util.Objects.requireNonNull;

/**
 * A thread that listens to messages in the queue and extracts
 * data from html blobs given selectors
 */
public class DataExtractionThread extends Thread {
    public final BlockingQueue<Message> messageQueue = new LinkedBlockingQueue<>();

    private final Optional<RateLimiter> rateLimiter;

    public DataExtractionThread(RateLimiter rateLimiter) {
        super();
        this.rateLimiter = Optional.of(requireNonNull(rateLimiter));
    }

    public DataExtractionThread() {
        super();
        this.rateLimiter = Optional.empty();
    }

    @Override
    public void run() {
        HandleMessageVisitor visitor = new HandleMessageVisitor(this.rateLimiter);
        while (true) {
            Message msg;
            while ((msg = this.messageQueue.poll()) != null) {
                // Handle the messages as they come in through the queue
                // and stop the thread if the message handler returns true
                if (msg.accept(visitor)) return;
            }
        }
    }
}
