package d_m.scraper.message;

public interface MessageVisitor<T> {
    T visitQuit(QuitMessage message);
    T visitExtractData(ExtractDataMessage message);
}
