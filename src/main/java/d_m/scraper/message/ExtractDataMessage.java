package d_m.scraper.message;

public final class ExtractDataMessage implements Message {
    @Override
    public <T> T accept(MessageVisitor<T> visitor) {
        return visitor.visitExtractData(this);
    }
}
