package d_m.scraper.message;

public final class QuitMessage implements Message {
    @Override
    public <T> T accept(MessageVisitor<T> visitor) {
        return visitor.visitQuit(this);
    }
}
