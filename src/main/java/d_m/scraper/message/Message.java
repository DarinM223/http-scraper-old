package d_m.scraper.message;

public interface Message {
    <T> T accept(MessageVisitor<T> visitor);
}
