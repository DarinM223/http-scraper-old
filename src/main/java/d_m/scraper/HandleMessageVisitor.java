package d_m.scraper;

import d_m.scraper.message.ExtractDataMessage;
import d_m.scraper.message.MessageVisitor;
import d_m.scraper.message.QuitMessage;

import java.util.Optional;

import static java.util.Objects.requireNonNull;

public class HandleMessageVisitor implements MessageVisitor<Boolean> {
    private final Optional<RateLimiter> rateLimiter;

    public HandleMessageVisitor(Optional<RateLimiter> rateLimiter) {
        this.rateLimiter = requireNonNull(rateLimiter);
    }

    @Override
    public Boolean visitExtractData(ExtractDataMessage message) {
        // TODO(DarinM223): extract data, then create a transaction that writes to disk and can rollback if failed halfway through
        return false;
    }

    @Override
    public Boolean visitQuit(QuitMessage message) {
        return true;
    }
}
