package d_m.scraper;

import d_m.config.Config;

import static java.util.Objects.requireNonNull;

public class WebScraper implements Scraper {
    private final Config config;

    public WebScraper(Config config) {
        this.config = requireNonNull(config);
    }
}
