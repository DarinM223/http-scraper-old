package d_m;

import d_m.config.Config;
import d_m.config.ConfigReader;
import d_m.scraper.Scraper;
import d_m.scraper.WebScraper;

import java.io.*;

public class Main {
    public static void main(String[] args) {
        if (args.length <= 0) {
            System.err.println("Need to include path to configuration file");
            return;
        }

        String path = args[0];
        ConfigReader cfgReader;
        try {
            cfgReader = new ConfigReader(path);
        } catch (FileNotFoundException e) {
            System.err.println("Error occurred when attempting to read file: " + e);
            return;
        }

        Config config = cfgReader.readConfig();
        Scraper scraper = new WebScraper(config);
    }
}
