package d_m.config;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;

import static java.util.Objects.requireNonNull;

public class ConfigReader extends InputStreamReader {
    public ConfigReader(String path) throws FileNotFoundException {
        super(new FileInputStream(requireNonNull(path)));
    }

    public Config readConfig() {
        throw new NotImplementedException();
    }
}
