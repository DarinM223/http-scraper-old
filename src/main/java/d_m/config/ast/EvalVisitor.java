package d_m.config.ast;

import d_m.config.ast.expr.*;
import d_m.config.ast.obj.*;
import d_m.config.ast.stmt.*;

import java.util.Iterator;
import java.util.Optional;

public class EvalVisitor implements ExprVisitor<Obj>, StmtVisitor<Void> {
    private final EnvObj globalEnv;
    private Optional<EnvObj> localEnv;
    private Optional<String> currentPage;

    public EvalVisitor() {
        this.globalEnv = new EnvObj();
        this.localEnv = Optional.empty();
        this.currentPage = Optional.empty();
    }

    /*
     * Expression evaluating implementations
     */

    @Override
    public Obj visitInt(IntExpr expr) {
        return new IntObj(expr.value);
    }

    @Override
    public Obj visitSelector(SelectorExpr expr) {
        return null;
    }

    @Override
    public Obj visitVar(VarExpr expr) {
        return null;
    }

    @Override
    public Obj visitFor(ForExpr expr) throws Exception {
        // TODO(DarinM223): fixme
        Obj collectionObj = expr.collection.accept(this);
        collectionObj.accept(new ObjVisitor<Void>() {
            @Override
            public Void visitArray(ArrayObj array) throws Exception {
                Iterator<Obj> objIterator = array.iterator();
                while (objIterator.hasNext()) {
                    // add block scope to environment
                    if (EvalVisitor.this.localEnv.isPresent()) {
                        EnvObj newEnv = new EnvObj(EvalVisitor.this.localEnv.get());
                        EvalVisitor.this.localEnv = Optional.of(newEnv);
                    } else {
                        EvalVisitor.this.localEnv = Optional.of(new EnvObj());
                    }

                    // bind for loop value in new scope
                    EvalVisitor.this.localEnv.get().set(expr.name, new VarEntry(objIterator.next()));

                    // evaluate in block scope
                    expr.body.accept(EvalVisitor.this);

                    // remove block scope
                    if (EvalVisitor.this.localEnv.isPresent()) {
                        EvalVisitor.this.localEnv = EvalVisitor.this.localEnv.get().parent();
                    }
                }
                return null;
            }

            @Override
            public Void visitInt(IntObj obj) throws Exception {
                throw new InvalidObjException();
            }

            @Override
            public Void visitEnv(EnvObj obj) throws Exception {
                throw new InvalidObjException();
            }
        });
        return null;
    }

    /*
     * Statement evaluating implementations
     */

    @Override
    public Void visitBinding(BindingStmt stmt) throws Exception {
        for (String name : stmt.bindings.keySet()) {
            Expr expr = stmt.bindings.get(name);
            Obj value = expr.accept(this);

            if (this.localEnv.isPresent()) {
                this.localEnv.get().set(name, new VarEntry(value));
            } else {
                this.globalEnv.set(name, new VarEntry(value));
            }
        }
        return null;
    }

    @Override
    public Void visitPrintf(PrintfStmt stmt) throws Exception {
        return null;
    }

    @Override
    public Void visitGoto(GotoStmt stmt) {
        return null;
    }

    @Override
    public Void visitSeq(SeqStmt stmt) {
        return null;
    }
}
