package d_m.config.ast;

import d_m.config.ast.expr.*;
import d_m.config.ast.stmt.*;

/**
 * Prints the expression by traversing the AST tree
 */
public class PrintVisitor implements ExprVisitor<Void>, StmtVisitor<Void> {
    private int indentLevel = 0;
    private int indentSpaces;

    public PrintVisitor(int indentSpaces) {
        this.indentSpaces = indentSpaces;
    }

    private String indentSpaces() {
        int capacity = this.indentLevel * this.indentSpaces;
        if (capacity == 0) {
            return "";
        }
        StringBuilder builder = new StringBuilder(capacity);
        for (int i = 0; i < capacity; i++) {
            builder.append(" ");
        }
        return builder.toString();
    }

    /*
     * Expression printing implementations
     */

    @Override
    public Void visitInt(IntExpr expr) {
        System.out.print(expr.value);
        return null;
    }

    @Override
    public Void visitSelector(SelectorExpr expr) {
        char prefix;
        switch (expr.type) {
            case CLASS:
                prefix = '.';
                break;
            case ID:
            default:
                prefix = '#';
                break;
        }
        System.out.print(prefix + expr.selectorName);
        return null;
    }

    @Override
    public Void visitFor(ForExpr expr) throws Exception {
        System.out.print("for " + expr.name + " in ");
        expr.collection.accept(this);
        System.out.println(" {");

        this.indentLevel += 1;
        expr.body.accept(this);
        this.indentLevel -= 1;

        System.out.print(this.indentSpaces() + "}");
        return null;
    }

    @Override
    public Void visitVar(VarExpr expr) {
        System.out.print(expr.name);
        return null;
    }

    /*
     * Statement printing implementations
     */

    @Override
    public Void visitBinding(BindingStmt stmt) throws Exception {
        System.out.print(this.indentSpaces());
        int keys = 0;
        for (String name : stmt.bindings.keySet()) {
            // Print variable names to bind and the expressions being bound to
            Expr value = stmt.bindings.get(name);
            System.out.print(name + " = ");
            value.accept(this);

            keys += 1;
            if (keys < stmt.bindings.size()) {
                System.out.print(", ");
            }
        }
        System.out.println();
        return null;
    }

    @Override
    public Void visitSeq(SeqStmt stmt) throws Exception {
        stmt.a.accept(this);
        stmt.b.accept(this);
        return null;
    }

    @Override
    public Void visitPrintf(PrintfStmt stmt) throws Exception {
        System.out.print(this.indentSpaces() + "printf \"" + stmt.format + "\"");
        for (Expr expr : stmt.expressions) {
            System.out.print(", ");
            expr.accept(this);
        }
        return null;
    }

    @Override
    public Void visitGoto(GotoStmt stmt) {
        System.out.println(this.indentSpaces() + "goto " + stmt.url);
        return null;
    }
}
