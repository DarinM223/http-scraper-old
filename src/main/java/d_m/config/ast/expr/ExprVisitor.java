package d_m.config.ast.expr;

/**
 * A visitor interface to walk through the expression nodes
 */
public interface ExprVisitor<T> {
    T visitSelector(SelectorExpr expr) throws Exception;
    T visitVar(VarExpr expr) throws Exception;
    T visitInt(IntExpr expr) throws Exception;
    T visitFor(ForExpr expr) throws Exception;
}
