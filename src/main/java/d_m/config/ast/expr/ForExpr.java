package d_m.config.ast.expr;

import d_m.config.ast.stmt.ScopedStmt;

import static java.util.Objects.requireNonNull;

/**
 * Iterate through fields of a collection like an array or a map
 * Syntax: for name in collection { }
 */
public final class ForExpr implements Expr {
    public final Expr collection;
    public final String name;
    public final ScopedStmt body;

    public ForExpr(Expr collection, String name, ScopedStmt body) {
        this.name = requireNonNull(name);
        this.collection = requireNonNull(collection);
        this.body = requireNonNull(body);
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) throws Exception {
        return visitor.visitFor(this);
    }
}
