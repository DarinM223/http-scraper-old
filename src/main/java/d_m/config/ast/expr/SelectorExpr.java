package d_m.config.ast.expr;

import static java.util.Objects.requireNonNull;

/**
 * Retrieve a value from a CSS selectorName from the current page
 */
public final class SelectorExpr implements Expr {
    public enum SelectorType {
        CLASS,
        ID,
    }

    public final String selectorName;
    public final SelectorType type;

    public SelectorExpr(String selector) {
        requireNonNull(selector);
        if (selector.length() > 0) {
            switch (selector.charAt(0)) {
                case '.':
                    this.type = SelectorType.CLASS;
                    break;
                case '#':
                default:
                    this.type = SelectorType.ID;
                    break;
            }
        } else {
            this.type = SelectorType.ID;
        }
        this.selectorName = selector.substring(1);
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) throws Exception {
        return visitor.visitSelector(this);
    }
}
