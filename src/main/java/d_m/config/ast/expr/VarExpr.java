package d_m.config.ast.expr;

import static java.util.Objects.requireNonNull;

/**
 * Retrieve the value of a variable name from either local or global scope
 */
public final class VarExpr implements Expr {
    public final String name;

    public VarExpr(String name) {
        this.name = requireNonNull(name);
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) throws Exception {
        return visitor.visitVar(this);
    }
}
