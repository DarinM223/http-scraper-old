package d_m.config.ast.expr;

public interface Expr {
    <T> T accept(ExprVisitor<T> visitor) throws Exception;
}
