package d_m.config.ast.expr;

public final class IntExpr implements Expr {
    public final int value;

    public IntExpr(int value) {
        this.value = value;
    }

    @Override
    public <T> T accept(ExprVisitor<T> visitor) throws Exception {
        return visitor.visitInt(this);
    }
}
