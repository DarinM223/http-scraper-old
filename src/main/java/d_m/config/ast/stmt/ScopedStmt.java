package d_m.config.ast.stmt;

public interface ScopedStmt {
    <T> T accept(StmtVisitor<T> visitor) throws Exception;
}
