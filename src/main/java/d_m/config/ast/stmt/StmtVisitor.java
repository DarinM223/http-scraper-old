package d_m.config.ast.stmt;

/**
 * A visitor interface to walk through the statement nodes
 */
public interface StmtVisitor<T> {
    T visitBinding(BindingStmt stmt) throws Exception;
    T visitSeq(SeqStmt stmt) throws Exception;
    T visitGoto(GotoStmt stmt) throws Exception;
    T visitPrintf(PrintfStmt stmt) throws Exception;
}
