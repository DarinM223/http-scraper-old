package d_m.config.ast.stmt;

import com.google.common.collect.ImmutableList;
import d_m.config.ast.expr.Expr;

import java.util.List;

public class PrintfStmt implements ScopedStmt {
    public final String format;
    public final ImmutableList<Expr> expressions;

    public PrintfStmt(String format, List<Expr> expressions) {
        this.format = format;
        this.expressions = ImmutableList.copyOf(expressions);
    }

    @Override
    public <T> T accept(StmtVisitor<T> visitor) throws Exception {
        return visitor.visitPrintf(this);
    }
}
