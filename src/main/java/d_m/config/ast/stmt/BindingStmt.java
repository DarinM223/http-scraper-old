package d_m.config.ast.stmt;

import com.google.common.collect.ImmutableMap;
import d_m.config.ast.expr.Expr;

import java.util.Map;

import static java.util.Objects.requireNonNull;

public final class BindingStmt implements ScopedStmt {
    // Variable -> Expression map
    public final ImmutableMap<String, Expr> bindings;

    public BindingStmt(Map<String, Expr> bindings) {
        this.bindings = ImmutableMap.copyOf(requireNonNull(bindings));
    }

    public BindingStmt(String name, Expr expr) {
        this.bindings = ImmutableMap.of(requireNonNull(name), requireNonNull(expr));
    }

    @Override
    public <T> T accept(StmtVisitor<T> visitor) throws Exception {
        return visitor.visitBinding(this);
    }
}
