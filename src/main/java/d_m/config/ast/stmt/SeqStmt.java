package d_m.config.ast.stmt;

import static java.util.Objects.requireNonNull;

public final class SeqStmt implements ScopedStmt {
    public final ScopedStmt a;
    public final ScopedStmt b;

    public SeqStmt(ScopedStmt a, ScopedStmt b) {
        this.a = requireNonNull(a);
        this.b = requireNonNull(b);
    }

    @Override
    public <T> T accept(StmtVisitor<T> visitor) throws Exception {
        return visitor.visitSeq(this);
    }
}
