package d_m.config.ast.stmt;

import static java.util.Objects.requireNonNull;

/**
 * Changes the current page to another page from a given URL
 */
public final class GotoStmt implements ScopedStmt {
    public final String url;

    public GotoStmt(String url) {
        this.url = requireNonNull(url);
    }

    @Override
    public <T> T accept(StmtVisitor<T> visitor) throws Exception {
       return visitor.visitGoto(this);
    }
}
