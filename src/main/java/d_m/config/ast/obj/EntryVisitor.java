package d_m.config.ast.obj;

public interface EntryVisitor<T> {
    T visitVar(VarEntry entry);
    T visitMethod(MethodEntry entry);
}
