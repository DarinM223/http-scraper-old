package d_m.config.ast.obj;


import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

public final class ArrayObj implements Obj {
    public final ImmutableList<Obj> array;

    public ArrayObj(int length, Obj init) {
        List<Obj> array = new ArrayList<>(length);
        Obj initObj = requireNonNull(init);
        for (int i = 0; i < length; i++) {
            array.add(initObj);
        }
        this.array = ImmutableList.copyOf(array);
    }

    public ArrayObj(List<Obj> array) {
        this.array = ImmutableList.copyOf(requireNonNull(array));
    }

    public Iterator<Obj> iterator() {
        return this.array.iterator();
    }

    @Override
    public <T> T accept(ObjVisitor<T> visitor) throws Exception {
        return visitor.visitArray(this);
    }
}
