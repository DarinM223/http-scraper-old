package d_m.config.ast.obj;

public final class IntObj implements Obj {
    public final int value;

    public IntObj(int value) {
        this.value = value;
    }

    @Override
    public <T> T accept(ObjVisitor<T> visitor) throws Exception {
        return visitor.visitInt(this);
    }
}
