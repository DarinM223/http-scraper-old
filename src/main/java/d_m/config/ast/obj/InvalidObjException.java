package d_m.config.ast.obj;

public class InvalidObjException extends Exception {
    public InvalidObjException() {
    }

    public InvalidObjException(String message) {
        super(message);
    }
}
