package d_m.config.ast.obj;

import com.google.common.collect.ImmutableList;
import d_m.config.ast.stmt.ScopedStmt;

import java.util.List;

import static java.util.Objects.requireNonNull;

public final class MethodEntry implements Entry {
    public final ScopedStmt body;
    public final ImmutableList<String> params;

    public MethodEntry(ScopedStmt body, List<String> params) {
        this.body = requireNonNull(body);
        this.params = ImmutableList.copyOf(requireNonNull(params));
    }

    @Override
    public <T> T accept(EntryVisitor<T> visitor) {
        return visitor.visitMethod(this);
    }
}
