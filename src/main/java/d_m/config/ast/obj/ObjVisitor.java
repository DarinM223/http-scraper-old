package d_m.config.ast.obj;

public interface ObjVisitor<T> {
    T visitInt(IntObj obj) throws Exception;
    T visitArray(ArrayObj obj) throws Exception;
    T visitEnv(EnvObj obj) throws Exception;
}
