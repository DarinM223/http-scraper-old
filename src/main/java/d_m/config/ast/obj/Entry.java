package d_m.config.ast.obj;

public interface Entry {
    <T> T accept(EntryVisitor<T> visitor);
}
