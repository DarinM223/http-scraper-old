package d_m.config.ast.obj;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public final class EnvObj implements Obj {
    private Optional<EnvObj> parent;
    private Map<String, Entry> table;

    public EnvObj() {
        this.parent = Optional.empty();
        this.table = new HashMap<>();
    }

    public EnvObj(EnvObj parent) {
        this.parent = Optional.of(parent);
    }

    public Optional<Entry> get(String key) {
        if (this.table.containsKey(key)) {
            return Optional.of(this.table.get(key));
        } else if (this.parent.isPresent()) {
            return this.parent.get().get(key);
        }
        return Optional.empty();
    }

    public void set(String key, Entry value) {
        this.table.put(key, value);
    }

    public Optional<EnvObj> parent() {
        return this.parent;
    }

    @Override
    public <T> T accept(ObjVisitor<T> visitor) throws Exception {
        return visitor.visitEnv(this);
    }
}
