package d_m.config.ast.obj;

import static java.util.Objects.requireNonNull;

public final class VarEntry implements Entry {
    public final Obj value;

    public VarEntry(Obj value) {
        this.value = requireNonNull(value);
    }

    @Override
    public <T> T accept(EntryVisitor<T> visitor) {
        return visitor.visitVar(this);
    }
}
