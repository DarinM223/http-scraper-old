package d_m.config.ast.obj;

public interface Obj {
    <T> T accept(ObjVisitor<T> visitor) throws Exception;
}
