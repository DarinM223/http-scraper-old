package d_m.config.graph;

public class GraphTask<T> implements Comparable<GraphTask<T>> {
    public final T data;
    public final int priority;

    public GraphTask(T data, int priority) {
        this.data = data;
        this.priority = priority;
    }

    @Override
    public int compareTo(GraphTask<T> o) {
        return Integer.compare(this.priority, o.priority);
    }
}
