package d_m.config.graph;

import java.util.PriorityQueue;

public class GraphNode<T> {
    public final GraphTask<T> task;
    public final int id;
    private PriorityQueue<GraphNode<T>> vertices;

    public GraphNode(int id, GraphTask<T> task) {
        this.id = id;
        this.task = task;
        this.vertices = new PriorityQueue<>();
    }

    public GraphNode(int id, GraphTask<T> task, int children) {
        this.id = id;
        this.task = task;
        this.vertices = new PriorityQueue<>(children);
    }

    public void addEdge(GraphNode<T> child) {
        this.vertices.add(child);
    }
}
