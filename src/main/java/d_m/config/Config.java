package d_m.config;

import d_m.config.ast.stmt.ScopedStmt;

import static java.util.Objects.requireNonNull;

public class Config {
    public final ScopedStmt ast;

    public Config(ScopedStmt ast) {
        this.ast = requireNonNull(ast);
    }
}
